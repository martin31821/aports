# Contributor: Adam Jensen <adam@acj.sh>
# Maintainer: Adam Jensen <adam@acj.sh>
pkgname=rbspy
pkgver=0.4.0
pkgrel=0
pkgdesc="Sampling profiler for Ruby"
url="https://rbspy.github.io/"
arch="all !armv7 !ppc64le !s390x !mips !mips64" # limited by cargo and build errors
license="MIT"
makedepends="cargo"
checkdepends="ruby"
source="$pkgname-$pkgver.tar.gz::https://github.com/rbspy/rbspy/archive/v$pkgver.tar.gz"

build() {
	cargo build --release --locked
}

check() {
	# Some tests need additional privileges
	cargo test --release --locked -- \
		--skip test_current_thread_address \
		--skip test_initialize_with_disallowed_process \
		--skip test_get_trace \
		--skip test_get_exec_trace \
		--skip test_spawn_record_children_subprocesses
}

package() {
	install -Dm755 "target/release/rbspy" "$pkgdir/usr/bin/rbspy"
}

sha512sums="3c62ea8fd91cf1a81c64c4bb1d3d4cca057bcb452b3d282057eb003c912df71eebebf349a5c13956197092214f226fb03b8e4c7b52e52f4679e19f5e11915465  rbspy-0.4.0.tar.gz"
