# Maintainer: Natanael Copa <ncopa@alpinelinux.org>

_flavor=lts
pkgname=linux-${_flavor}
pkgver=5.10.26
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=1
pkgdesc="Linux lts kernel"
url="https://www.kernel.org"
depends="mkinitfs"
_depends_dev="perl gmp-dev elfutils-dev bash flex bison"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any openssl-dev
	diffutils findutils"
options="!strip"
_config=${config:-config-lts.${CARCH}}
install=
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz
	0002-powerpc-config-defang-gcc-check-for-stack-protector-.patch

	0007-pci-hotplug-declare-IDT-bridge-as-hotpluggabl-bridge.patch
	0008-pci-spr2803-quirk-to-fix-class-ID.patch
	bonding.patch
	ampere-mt-jade.patch

	config-lts.aarch64
	config-lts.armv7
	config-lts.x86
	config-lts.x86_64
	config-lts.ppc64le
	config-lts.s390x
	config-lts.mips64

	config-virt.aarch64
	config-virt.armv7
	config-virt.ppc64le
	config-virt.x86
	config-virt.x86_64
	"
subpackages="$pkgname-dev:_dev:$CBUILD_ARCH"
_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors ${_f}"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-${_f}::$CBUILD_ARCH linux-${_f}-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

if [ "${pkgver%.0}" = "$pkgver" ]; then
	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz"
fi
arch="all !armhf"
license="GPL-2.0"

_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
mips*) _carch="mips" ;;
ppc*) _carch="powerpc" ;;
s390*) _carch="s390" ;;
esac

prepare() {
	local _patch_failed=
	cd "$srcdir"/linux-$_kernver
	if [ "$_kernver" != "$pkgver" ]; then
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N
	fi

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.${CARCH}
		local _builddir="$srcdir"/build-$i.$CARCH
		mkdir -p "$_builddir"
		echo "-$pkgrel-$i" > "$_builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$_builddir"/.config
		make -C "$srcdir"/linux-$_kernver \
			O="$_builddir" \
			ARCH="$_carch" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$srcdir"/build-$i.$CARCH
		make ARCH="$_carch" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=${pkgver}-${pkgrel}-${_buildflavor}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/${_abi_release}/build \
		"$_outdir"/lib/modules/${_abi_release}/source
	rm -rf "$_outdir"/lib/firmware

	install -D include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package lts "$pkgdir"
}

# subflavors install in $subpkgdir
virt() {
	_package virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=${pkgver}-${pkgrel}-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-${_abi_release}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.${CARCH} "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine

	make -j1 -C "$srcdir"/linux-$_kernver O="$dir" ARCH="$_carch" \
		syncconfig prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the soruces
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$srcdir"/linux-$_kernver
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find $(find arch -name include -type d -print) -type f \
		| cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	mkdir -p "$subpkgdir"/lib/modules/${_abi_release}
	ln -sf /usr/src/linux-headers-${_abi_release} \
		"$subpkgdir"/lib/modules/${_abi_release}/build
}

sha512sums="95bc137d0cf9148da6a9d1f1a878698dc27b40f68e22c597544010a6c591ce1b256f083489d3ff45ff77753289b535135590194d88ef9f007d0ddab3d74de70e  linux-5.10.tar.xz
d19365fe94431008768c96a2c88955652f70b6df6677457ee55ee95246a64fdd2c6fed9b3bef37c29075178294a7fc91f148ead636382530ebfa822be4ad8c2f  0002-powerpc-config-defang-gcc-check-for-stack-protector-.patch
ca5aafac37e0b5f3fcbaf801e12f98beb58ffaf1d8c88f76caff22b059831869b4094e7fdcb6d6860422d6b2d036e072caff460e1feb84bd04d10740ad56265b  0007-pci-hotplug-declare-IDT-bridge-as-hotpluggabl-bridge.patch
cbe85cf34e8420c91d2276c2d2aa0ab5023af68e57a1fa613f073f16a76766c67f585eda71c28f232bd0625e0dc8275a9eddc95f49409205dc0dbcc28c9fac1c  0008-pci-spr2803-quirk-to-fix-class-ID.patch
3b89908ec95c93a6fd0b3e8d4cf9a0c6787f204018b733b01732464786d2099bbd884cbc9a3228ae46bd69ddb97123d726ba122699204cdda009edaa301651cd  bonding.patch
16b2d5b0255b37075ba894fc797673d633395907ce0b93400c5a8bd05b512b5cd040b91000fa41f9240d42afc664a69206597d1e3f754a1aa64b9be21a67f5c6  ampere-mt-jade.patch
4d342d2043f1dc5a52628d2f267c57da71fe32f018123e169c9b36e4ba986e966f802f46d82fe8b49c9afe5e90033c62e3ea3f03efe003e489db30b65adc2344  config-lts.aarch64
1711108599b86b645566cc34217a7333d757f97ae545902e07e743108852258740de015c5c54d829480aaf448ad7898c2cf3084f60cb3faecd56ea20b23ed857  config-lts.armv7
97e472f493dd976489e224a3979e418f6697f9d9b86958b02b96f9171d1cda9876cb0083190f8522fd2cf3fdee913d10108201c57f52a2bf109d6d08b256d192  config-lts.x86
f79d8fce964439c36fcd8d77b48f05487dfd647341529ccde67716e29bd22f7acad962721cbfbe45395c50b68ea9b4c2dfb77bca1bf658e6102efa37749f3605  config-lts.x86_64
56214317983e4c37608c089e77370aca8400367b95877831ee57ce9c5d863633748b3a990cb9d08b61c2e8fd40aed5eff5affdc64f884637faa982debedd9caf  config-lts.ppc64le
5fa3d48a178e86231a004cd68d3662155bbf90e745242c12cf32ecd6ef6d89ba5152c14ae307b49d0cbf9abffca9c81e688771af2c7b106aac7d9fadae72e014  config-lts.s390x
190e90a34cc25e57024c1da62b6d787194b5a0b2c96c6fb5b44762be7b83b02e49394d16382214b986fc9dafb97463595cd43a6107c459906b73494f3ced968c  config-lts.mips64
7f41477ecec63c497312ce65ad5d952b954f7e26ffca9a557e8500e04fe1790c37e5561717c912c27da89a5b9c01d431b69261c31e98d7a1f158094483cba1b5  config-virt.aarch64
bed4b8fb7514a9086ab23a685f59adbfdd7cda3e33f52a3cf64b43cb175219e2e7e0d6b94ba28f66d4fc402c46c95ac5594f08ca5d0d9287deb922a9ed400807  config-virt.armv7
73001637baa05ec07f47dd608125ee4deac445c35d796ababcab2f80dfd2687f9ec461bd6bcbba47193ab1c0cc9a7719775c3af51b08400667b3c31efb51a1ae  config-virt.ppc64le
a19dc1817a097321e16559684ea54a3247054d11ba9152b13ff54e2690421eafe722963cda457673f5a5cd7c2139613d3f7e9b0756a113fb91b912e83bf6e96b  config-virt.x86
7a45e09b58a173f28cdc56a0345b0f580f42900ee1c9ea2605eac3bd2488dbd0c4b524de9f7d045491a3a3de34730d12f68ab8a4832339cfe383a143cb3b5aa8  config-virt.x86_64
0938834855a70ee5a77d7fc45f3f005983d0ad726f9d6367297970d885f19c433cf09c8592f74ba5c2861bf7eeb303455dd7ecebf4ff4c31baeff7c96357b9be  patch-5.10.26.xz"
